CREATE TABLE notas(
	nota1 REAL,
  	nota2 REAL,
  	nota3 REAL,
  	media REAL,
  	aluno_id BIGINT(20) NOT NULL,
  	FOREIGN KEY (aluno_id) REFERENCES aluno(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
