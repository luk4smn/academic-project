CREATE TABLE professor(
	id BIGINT(20) AUTO_INCREMENT,
	matricula INT NOT NULL,
	nome VARCHAR(255) NOT NULL,
	curso VARCHAR(255) NOT NULL,
	rua VARCHAR(255) NOT NULL,
	numero VARCHAR(8) NOT NULL,
	cep VARCHAR(14) NOT NULL,
	cidade VARCHAR(50) NOT NULL,
	estado VARCHAR(50) NOT NULL,
	pais VARCHAR(50) NOT NULL,
	
	PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO professor (matricula, nome, curso, rua, numero, cep, cidade, estado, pais)
VALUES(1560211539,'Moisés SeaOpen.', 'CC', 'Rua Nascimento Sebastião', '666', '44700-000', 'Campina Grande', 'Paraíba', 'Brasil');