CREATE TABLE projeto(
	id BIGINT(20) AUTO_INCREMENT,
  	titulo VARCHAR(255) NOT NULL,
  	area_do_projeto VARCHAR(255) NOT NULL,
  	resumo VARCHAR(3000) NOT NULL,
  	palavra_chave1 VARCHAR(50) NOT NULL,
  	palavra_chave2 VARCHAR(50) NOT NULL,
  	palavra_chave3 VARCHAR(50) NOT NULL,
  	url_doc VARCHAR(255) NOT NULL,
  	professor_id BIGINT(20) NOT NULL,
  	aluno_id BIGINT(20) NOT NULL,
  	
  	PRIMARY KEY (id),
  	
  	FOREIGN KEY (professor_id) REFERENCES professor(id),
  	FOREIGN KEY (aluno_id) REFERENCES aluno(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO projeto(titulo, area_do_projeto, resumo, palavra_chave1, palavra_chave2, palavra_chave3, url_doc, aluno_id, professor_id)
VALUES('Machine Learning for dummies','Machine Learning', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna', 'ronaldo','zico', 'beiçola', 'https://expoforest.com.br/wp-content/uploads/2017/05/exemplo.pdf', 1, 1);
