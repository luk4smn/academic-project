CREATE TABLE aluno(
	id BIGINT(20) AUTO_INCREMENT,
	matricula INT NOT NULL,
	nome_completo VARCHAR(255) NOT NULL,
	cpf VARCHAR(13) NOT NULL,
	curso VARCHAR(255) NOT NULL,
	rua VARCHAR(255) NOT NULL,
	numero VARCHAR(8) NOT NULL,
	cep VARCHAR(14) NOT NULL,
	cidade VARCHAR(50) NOT NULL,
	estado VARCHAR(50) NOT NULL,
	pais VARCHAR(50) NOT NULL,
	
	PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO aluno (matricula, nome_completo, cpf, curso, rua, numero, cep, cidade, estado, pais)
VALUES(131088670,'Nathan F.', '02618984501','CC', 'Rua Sebastião Nascimento', '500', '44700-000', 'Campina Grande', 'Paraíba', 'Brasil');