package com.uepb.academic.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.uepb.academic.model.Notas;

public interface NotasRepository extends JpaRepository<Notas, Long> {
	Optional<Notas> findByAlunoId(Long aluno_id);
	
	@Modifying // usado p/ update e delete
	@Query("update Notas u set u.nota1 = ?1, u.nota2 = ?2, u.nota3 = ?3, u.media = ?4  where u.aluno.id = ?5")
	void doUpdate(Float nota1, Float nota2, Float nota3, Float media, Long aluno_id);
	
}
