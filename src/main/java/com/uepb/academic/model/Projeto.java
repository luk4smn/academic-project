package com.uepb.academic.model;

import javax.persistence.*;
import com.sun.istack.*;


import java.io.Serializable;
import java.util.Objects;

@Entity
public class Projeto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String titulo;
	private String areaDoProjeto;
	private String resumo;
	private String palavraChave1,palavraChave2, palavraChave3;
	private String urlDoc;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "professor_id")
	private Professor professor;
	
	@ManyToOne
	@JoinColumn(name = "aluno_id")
	private Aluno aluno;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAreaDoProjeto() {
		return areaDoProjeto;
	}

	public void setAreaDoProjeto(String areaDoProjeto) {
		this.areaDoProjeto = areaDoProjeto;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public String getPalavraChave1() {
		return palavraChave1;
	}

	public void setPalavraChave1(String palavraChave1) {
		this.palavraChave1 = palavraChave1;
	}

	public String getPalavraChave2() {
		return palavraChave2;
	}

	public void setPalavraChave2(String palavraChave2) {
		this.palavraChave2 = palavraChave2;
	}

	public String getPalavraChave3() {
		return palavraChave3;
	}

	public void setPalavraChave3(String palavraChave3) {
		this.palavraChave3 = palavraChave3;
	}

	public String getUrlDoc() {
		return urlDoc;
	}

	public void setUrlDoc(String urlDoc) {
		this.urlDoc = urlDoc;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Projeto projeto = (Projeto) o;
		return
			id.equals(projeto.id);

	}

	@Override
	public int hashCode() {
		return Objects.hash(id, titulo);
	}
	
	
	
}
