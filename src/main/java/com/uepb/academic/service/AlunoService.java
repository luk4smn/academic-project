package com.uepb.academic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uepb.academic.model.Aluno;
import com.uepb.academic.repository.AlunoRepository;


@Service
public class AlunoService {
	@Autowired
	private AlunoRepository repository;
	
	public List<Aluno> getAll(){
		return repository.findAll();
	}

	public Optional<Aluno> getById(Long id){
		return repository.findById(id);
	}

	public Optional<Aluno> create(Aluno aluno){

		if(repository.findById(aluno.getId()).isPresent()){
			return null;
		}else{
			repository.save(aluno);
			return repository.findById(aluno.getId());
		}
	}

	public Optional<Aluno> update(Long id, Aluno aluno){
		Optional<Aluno> actual = repository.findById(id);
		if(!actual.isPresent()) return actual;

		repository.save(aluno);
		return repository.findById(aluno.getId());
	}
	

	public Optional<Aluno> delete(Long id){
		repository.deleteById(id);
		return repository.findById(id);
	}

}
