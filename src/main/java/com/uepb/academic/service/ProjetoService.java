package com.uepb.academic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uepb.academic.model.Projeto;
import com.uepb.academic.repository.ProjetoRepository;

@Service
public class ProjetoService {
	@Autowired
	private ProjetoRepository repository;
	
	public List<Projeto> getAll(){
		return repository.findAll();
	}

	public Optional<Projeto> getById(Long id){
		return repository.findById(id);
	}

	public Optional<Projeto> create(Projeto projeto){

		if(repository.findById(projeto.getId()).isPresent()){
			return null;
		}else{
			repository.save(projeto);
			return repository.findById(projeto.getId());
		}
	}

	public Optional<Projeto> update(Long id, Projeto projeto){
		Optional<Projeto> actual = repository.findById(id);
		if(!actual.isPresent()) return actual;

		repository.save(projeto);
		return repository.findById(projeto.getId());
	}
	

	public Optional<Projeto> delete(Long id){
		repository.deleteById(id);
		return repository.findById(id);
	}

}
