package com.uepb.academic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uepb.academic.model.Professor;
import com.uepb.academic.repository.ProfessorRepository;


@Service
public class ProfessorService {
	@Autowired
	private ProfessorRepository repository;

	public List<Professor> getAll(){
		return repository.findAll();
	}

	public Optional<Professor> getById(Long id){
		return repository.findById(id);
	}
	
	public Optional<Professor> create(Professor professor){

		if(repository.findById(professor.getId()).isPresent()){
			return null;
		}else{
			repository.save(professor);
			return repository.findById(professor.getId());
		}
	}

	public Optional<Professor> update(Long id, Professor professor){
		Optional<Professor> actual = repository.findById(id);
		if(!actual.isPresent()) return actual;

		repository.save(professor);
		return repository.findById(professor.getId());
	}
	
	public Optional<Professor> delete(Long id){
		repository.deleteById(id);
		return repository.findById(id);
	}

}
