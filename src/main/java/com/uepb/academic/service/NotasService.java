package com.uepb.academic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uepb.academic.model.Notas;
import com.uepb.academic.repository.NotasRepository;

@Service
@Transactional
public class NotasService {
	@Autowired
	private NotasRepository repository;
	
	public List<Notas> getAll(){
		return repository.findAll();
	}
	
	public Optional<Notas> getByAlunoId(Long aluno_id){
		return repository.findByAlunoId(aluno_id);
	}
	
	public Optional<Notas> create(Notas obj){

		if(repository.findByAlunoId(obj.getAluno().getId()).isPresent()){
			return null;
		}else{
			obj.setMedia((obj.getNota1() + obj.getNota2() + obj.getNota3()) / 3);
			
			repository.save(obj);
			return repository.findByAlunoId(obj.getAluno().getId());
		}
	}

	public Optional<Notas> update(Long aluno_id, Notas obj){
		Optional<Notas> actual = repository.findByAlunoId(aluno_id);
		if(!actual.isPresent()) return actual;

		obj.setMedia((obj.getNota1() + obj.getNota2() + obj.getNota3()) / 3);
		
		repository.doUpdate(obj.getNota1(), obj.getNota2(), obj.getNota3(), obj.getMedia(), aluno_id);
		
		return repository.findByAlunoId(aluno_id);
	}
	

	public Optional<Notas> delete(Long aluno_id){
		repository.deleteById(aluno_id);
		return repository.findByAlunoId(aluno_id);
	}


}
