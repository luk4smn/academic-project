package com.uepb.academic.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uepb.academic.model.Projeto;
import com.uepb.academic.service.ProjetoService;

@RestController
@RequestMapping("/projetos")
public class ProjetoController {
	
	@Autowired
	private ProjetoService service;
	
	@GetMapping
	public ResponseEntity<List<?>> getAll() 
	{
		return new ResponseEntity<List<?>>(service.getAll(),HttpStatus.OK);
	}
		
	@GetMapping("/{id}")
	private ResponseEntity<Optional<?>> getById(@PathVariable Long id)
	{
		if(service.getById(id).isPresent()){
			return new ResponseEntity<Optional<?>>(service.getById(id), HttpStatus.OK);
		}else{
			return new ResponseEntity<Optional<?>>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping
	public ResponseEntity<Projeto> create(@RequestBody Projeto obj) 
	{
		if(service.create(obj).isPresent()){
			return new ResponseEntity<Projeto>(HttpStatus.CREATED);
		}else{
			return new ResponseEntity<Projeto>(HttpStatus.NOT_IMPLEMENTED);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Projeto> update(@PathVariable Long id, @RequestBody Projeto obj)
	{
		if(service.getById(id).isPresent()){
			return new ResponseEntity<Projeto>(service.update(id, obj).get(), HttpStatus.OK);
		}else{
			return new ResponseEntity<Projeto>(HttpStatus.NOT_FOUND);
		}
	}
}
