package com.uepb.academic.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uepb.academic.model.Aluno;
import com.uepb.academic.service.AlunoService;


@RestController
@RequestMapping("/alunos")
public class AlunoController {
	
	@Autowired
	private AlunoService service;
	
	@GetMapping
	public ResponseEntity<List<?>> getAll() 
	{
		return new ResponseEntity<List<?>>(service.getAll(),HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	private ResponseEntity<Optional<?>> getById(@PathVariable Long id)
	{
		if(service.getById(id).isPresent()){
			return new ResponseEntity<Optional<?>>(service.getById(id), HttpStatus.OK);
		}else{
			return new ResponseEntity<Optional<?>>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping
	public ResponseEntity<Aluno> create(@RequestBody Aluno obj) 
	{
		if(service.create(obj).isPresent()){
			return new ResponseEntity<Aluno>(HttpStatus.CREATED);
		}else{
			return new ResponseEntity<Aluno>(HttpStatus.NOT_IMPLEMENTED);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Aluno> update(@PathVariable Long id, @RequestBody Aluno obj)
	{
		if(service.getById(id).isPresent()){
			return new ResponseEntity<Aluno>(service.update(id, obj).get(), HttpStatus.OK);
		}else{
			return new ResponseEntity<Aluno>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable Long id)
	{
		Optional<Aluno> search = service.getById(id);

		if(search.isPresent()){
			service.delete(search.get().getId());
			return new ResponseEntity(HttpStatus.OK);
		}else{
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}

}
